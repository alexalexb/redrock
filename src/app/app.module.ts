import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { PageTwoComponent } from './views/page-two/page-two.component';
import { PageOneComponent } from './views/page-one/page-one.component';
import { UserBadgeComponent } from './components/user-badge/user-badge.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    PageTwoComponent,
    PageOneComponent,
    UserBadgeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
